import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmeksListComponent } from './filmeks-list/filmeks-list.component';
import { CreateFilmekComponent } from './create-filmek/create-filmek.component';
import { SearchFilmeksComponent } from './search-filmeks/search-filmeks.component';
import { SztaroksListComponent } from './sztaroks-list/sztaroks-list.component';
import { CreateSztarokComponent } from './create-sztarok/create-sztarok.component';
import { CvDetailsComponent } from './cv-details/cv-details.component';
import { CvsListComponent } from './cvs-list/cvs-list.component';


const routes: Routes = [
    { path: '', redirectTo: 'filmek', pathMatch: 'full' },
    { path: 'filmek', component: FilmeksListComponent },
    { path: 'add', component: CreateFilmekComponent },
    { path: 'findbymegjelenes', component: SearchFilmeksComponent },
    { path: 'sztarok', component: SztaroksListComponent },
    { path: 'addsztar', component: CreateSztarokComponent},
    { path: 'cv', component: CvsListComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
