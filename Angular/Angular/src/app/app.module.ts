import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CreateSztarokComponent } from './create-sztarok/create-sztarok.component';
import { CreateFilmekComponent } from './create-filmek/create-filmek.component';
import { FilmekDetailsComponent } from './filmek-details/filmek-details.component';
import { FilmeksListComponent } from './filmeks-list/filmeks-list.component';
import { SearchFilmeksComponent } from './search-filmeks/search-filmeks.component';
import { SztarokDetailsComponent } from './sztarok-details/sztarok-details.component';
import { SztaroksListComponent } from './sztaroks-list/sztaroks-list.component';
import { CvDetailsComponent } from './cv-details/cv-details.component';
import { CvsListComponent } from './cvs-list/cvs-list.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CreateFilmekComponent,
    FilmekDetailsComponent,
    FilmeksListComponent,
    SearchFilmeksComponent,
    SztarokDetailsComponent,
    SztaroksListComponent,
    CreateSztarokComponent,
    CvDetailsComponent,
    CvsListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
