import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFilmekComponent } from './create-filmek.component';

describe('CreateFilmekComponent', () => {
  let component: CreateFilmekComponent;
  let fixture: ComponentFixture<CreateFilmekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFilmekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFilmekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
