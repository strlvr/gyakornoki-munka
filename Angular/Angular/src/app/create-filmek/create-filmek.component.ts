import { Component, OnInit, ElementRef ,ViewChild} from '@angular/core'; 
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

import { Filmek } from '../services/filmek'
import { FilmekService } from '../services/filmek.service';

@Component({
  selector: 'create-filmek',
  templateUrl: './create-filmek.component.html',
  styleUrls: ['./create-filmek.component.css']
})
export class CreateFilmekComponent implements OnInit {
  

  filmek: Filmek = new Filmek();
  submitted = false;

  constructor(private filmekService: FilmekService) { }

  ngOnInit() {
  }

  newFilmek(): void {
    this.submitted = false;
    this.filmek = new Filmek();
  }

  save() {
    this.filmekService.createFilmek(this.filmek)
      .subscribe(data => console.log(data), error => console.log(error));
    this.filmek = new Filmek();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
