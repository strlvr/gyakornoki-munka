import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSztarokComponent } from './create-sztarok.component';

describe('CreateSztarokComponent', () => {
  let component: CreateSztarokComponent;
  let fixture: ComponentFixture<CreateSztarokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSztarokComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSztarokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
