import { Component, OnInit, ElementRef ,ViewChild} from '@angular/core';  
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

import { Sztarok } from '../services/sztarok';
import { SztarokService } from '../services/sztarok.service';

@Component({
  selector: 'create-sztarok',
  templateUrl: './create-sztarok.component.html',
  styleUrls: ['./create-sztarok.component.css']
})
export class CreateSztarokComponent implements OnInit {

  sztarok: Sztarok = new Sztarok();
  submitted = false;


  constructor(private sztarokService: SztarokService) { }

  ngOnInit() {
  }

  newSztarok(): void {
    this.submitted = false;
    this.sztarok = new Sztarok();
  }

  save() {
    this.sztarokService.createSztarok(this.sztarok)
      .subscribe(data => console.log(data), error => console.log(error));
    this.sztarok = new Sztarok();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
