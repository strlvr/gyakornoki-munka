import { Component, OnInit, Input } from '@angular/core';
import { CvService } from '../services/cv.service';
import { Cv } from '../services/cv';

import { CvsListComponent } from '../cvs-list/cvs-list.component';

@Component({
  selector: 'cv-details',
  templateUrl: './cv-details.component.html',
  styleUrls: ['./cv-details.component.css']
})
export class CvDetailsComponent implements OnInit {

  @Input() cv: Cv;

  constructor(private cvService: CvService, private listComponent: CvsListComponent) { }

  ngOnInit() {
  }

 

}