import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CvService {

  private baseUrl = 'http://localhost:8080/api/cvs';

  constructor(private http: HttpClient) { }

  getCv(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createCv(cv: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/create`, cv);
  }

  updateCv(id: string, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  getCvsList(){
    return this.http.get('http://localhost:8080/api/cvs');
  }

  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }
}
