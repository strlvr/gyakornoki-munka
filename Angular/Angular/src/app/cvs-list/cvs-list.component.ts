import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  

import { CvService } from '../services/cv.service';
import { Cv } from '../services/cv';

@Component({
  selector: 'cvs-list',
  templateUrl: './cvs-list.component.html',
  styleUrls: ['./cvs-list.component.css']
})
export class CvsListComponent implements OnInit {

  cvs$;

  constructor(private cvService: CvService) { }

  ngOnInit() {
    this.cvService.getCvsList().subscribe(
      data=>{
        this.cvs$ = data;
      }
    )
  }

  public captureScreen()  
  {  
    var data = document.getElementById('cv');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('NDcv.pdf'); // Generated PDF   
    });  
  }  


  
}
