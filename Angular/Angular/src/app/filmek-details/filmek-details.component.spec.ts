import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmekDetailsComponent } from './filmek-details.component';

describe('FilmekDetailsComponent', () => {
  let component: FilmekDetailsComponent;
  let fixture: ComponentFixture<FilmekDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmekDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmekDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
