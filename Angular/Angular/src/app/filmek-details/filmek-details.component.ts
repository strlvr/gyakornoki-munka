import { Component, ViewChild, OnInit, Input, ElementRef } from '@angular/core';
import { FilmekService } from '../services/filmek.service';
import { Filmek } from '../services/filmek';

import { FilmeksListComponent } from '../filmeks-list/filmeks-list.component';

@Component({
  selector: 'filmek-details',
  templateUrl: './filmek-details.component.html',
  styleUrls: ['./filmek-details.component.css']
})
export class FilmekDetailsComponent implements OnInit {

  @Input() filmek: Filmek;

  constructor(private filmekService: FilmekService, private listComponent: FilmeksListComponent) { }

  ngOnInit() {
  }



  updateActive(isActive: boolean) {
    this.filmekService.updateFilmek(this.filmek.id,
      { cim: this.filmek.cim, megjelenes: this.filmek.megjelenes})
      .subscribe(
        data => {
          console.log(data);
          this.filmek = data as Filmek;
        },
        error => console.log(error));
  }

  deleteFilmek() {
    this.filmekService.deleteFilmek(this.filmek.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }


  newFilmek(){
    
  }
}
