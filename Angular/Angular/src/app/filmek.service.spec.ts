import { TestBed, inject } from '@angular/core/testing';

import { FilmekService } from './filmek.service';

describe('FilmekService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilmekService]
    });
  });

  it('should be created', inject([FilmekService], (service: FilmekService) => {
    expect(service).toBeTruthy();
  }));
});
