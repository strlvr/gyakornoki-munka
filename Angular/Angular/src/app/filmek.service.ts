import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmekService {

  private baseUrl = 'http://localhost:8080/api/filmeks';

  constructor(private http: HttpClient) { }

  getFilmek(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createFilmek(filmek: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/create`, filmek);
  }

  updateFilmek(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteFilmek(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getFilmeksList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getFilmeksByMegjelenes(megjelenes: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/megjelenes/${megjelenes}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }
}
