import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmeksListComponent } from './filmeks-list.component';

describe('FilmeksListComponent', () => {
  let component: FilmeksListComponent;
  let fixture: ComponentFixture<FilmeksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmeksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmeksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
