import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { FilmekService } from '../services/filmek.service';
import { Filmek } from '../services/filmek';

@Component({
  selector: 'filmeks-list',
  templateUrl: './filmeks-list.component.html',
  styleUrls: ['./filmeks-list.component.css']
})
export class FilmeksListComponent implements OnInit {

  filmeks: Observable<Filmek[]>;

  constructor(private filmekService: FilmekService) { }

  ngOnInit() {
    this.reloadData();
  }

  deleteFilmeks() {
    this.filmekService.deleteAll()
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log('ERROR: ' + error));
  }

  reloadData() {
    this.filmeks = this.filmekService.getFilmeksList();
  }
}
