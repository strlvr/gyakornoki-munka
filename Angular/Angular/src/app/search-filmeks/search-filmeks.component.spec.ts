import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFilmeksComponent } from './search-filmeks.component';

describe('SearchFilmeksComponent', () => {
  let component: SearchFilmeksComponent;
  let fixture: ComponentFixture<SearchFilmeksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFilmeksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFilmeksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
