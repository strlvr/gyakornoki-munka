import { Component, OnInit } from '@angular/core';
import { Filmek } from '../services/filmek';
import { FilmekService } from '../services/filmek.service';

@Component({
  selector: 'search-filmeks',
  templateUrl: './search-filmeks.component.html',
  styleUrls: ['./search-filmeks.component.css']
})
export class SearchFilmeksComponent implements OnInit {

  megjelenes: number;
  filmeks: Filmek[];

  constructor(private dataService: FilmekService) { }

  ngOnInit() {
    this.megjelenes = 0;
  }

  private searchFilmeks() {
    this.dataService.getFilmeksByMegjelenes(this.megjelenes)
      .subscribe(filmeks => this.filmeks = filmeks);
  }

  onSubmit() {
    this.searchFilmeks();
  }
}
