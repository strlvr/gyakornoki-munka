import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SztarokDetailsComponent } from './sztarok-details.component';

describe('SztarokDetailsComponent', () => {
  let component: SztarokDetailsComponent;
  let fixture: ComponentFixture<SztarokDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SztarokDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SztarokDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
