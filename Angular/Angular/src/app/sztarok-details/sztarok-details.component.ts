import { Component, OnInit, Input } from '@angular/core';
import { SztarokService } from '../services/sztarok.service';
import { Sztarok } from '../services/sztarok';

import { SztaroksListComponent } from '../sztaroks-list/sztaroks-list.component';

@Component({
  selector: 'sztarok-details',
  templateUrl: './sztarok-details.component.html',
  styleUrls: ['./sztarok-details.component.css']
})
export class SztarokDetailsComponent implements OnInit {

  @Input() sztarok: Sztarok;

  constructor(private sztarokService: SztarokService, private listComponent: SztaroksListComponent) { }

  ngOnInit() {
  }


  deleteSztarok() {
    this.sztarokService.deleteSztarok(this.sztarok.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }

  newSztarok(){
    
  }
}
