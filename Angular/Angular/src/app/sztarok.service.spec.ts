import { TestBed, inject } from '@angular/core/testing';

import { SztarokService } from './sztarok.service';

describe('SztarokService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SztarokService]
    });
  });

  it('should be created', inject([SztarokService], (service: SztarokService) => {
    expect(service).toBeTruthy();
  }));
});
