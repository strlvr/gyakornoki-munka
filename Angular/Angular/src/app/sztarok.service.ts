import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SztarokService {

  private baseUrl = 'http://localhost:8080/api/sztaroks';

  constructor(private http: HttpClient) { }

  getSztarok(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createSztarok(sztarok: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/create`, sztarok);
  }

  updateSztarok(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteSztarok(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getSztaroksList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }


  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }
}
