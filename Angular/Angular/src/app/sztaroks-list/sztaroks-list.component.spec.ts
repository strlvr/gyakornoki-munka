import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SztaroksListComponent } from './sztaroks-list.component';

describe('SztaroksListComponent', () => {
  let component: SztaroksListComponent;
  let fixture: ComponentFixture<SztaroksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SztaroksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SztaroksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
