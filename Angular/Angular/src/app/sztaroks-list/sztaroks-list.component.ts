import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { SztarokService } from '../services/sztarok.service';
import { Sztarok } from '../services/sztarok';

@Component({
  selector: 'sztaroks-list',
  templateUrl: './sztaroks-list.component.html',
  styleUrls: ['./sztaroks-list.component.css']
})
export class SztaroksListComponent implements OnInit {

  sztaroks: Observable<Sztarok[]>;

  constructor(private sztarokService: SztarokService) { }

  ngOnInit() {
    this.reloadData();
  }

  deleteSztaroks() {
    this.sztarokService.deleteAll()
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log('ERROR: ' + error));
  }

  reloadData() {
    this.sztaroks = this.sztarokService.getSztaroksList();
  }
}
