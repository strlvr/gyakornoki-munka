import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFilmekComponent } from './update-filmek.component';

describe('UpdateFilmekComponent', () => {
  let component: UpdateFilmekComponent;
  let fixture: ComponentFixture<UpdateFilmekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFilmekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFilmekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
