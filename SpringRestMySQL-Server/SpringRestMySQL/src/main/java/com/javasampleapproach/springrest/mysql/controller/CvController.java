package com.javasampleapproach.springrest.mysql.controller;

import java.util.ArrayList;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.model.Cv;
import com.javasampleapproach.springrest.mysql.repo.CvRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CvController {

	@Autowired
	CvRepository repository;

	@GetMapping("/cvs")
	public List<Cv> getAllCvs() {
		System.out.println("osszes Cv");
		
		List<Cv> cvs = new ArrayList<>();
		repository.findAll().forEach(cvs::add);
		
		return cvs;
	}
	
	

}
