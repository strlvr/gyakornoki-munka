package com.javasampleapproach.springrest.mysql.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.model.Filmek;
import com.javasampleapproach.springrest.mysql.repo.FilmekRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class FilmekController {

	@Autowired
	FilmekRepository repository;

	@GetMapping("/filmeks")
	public List<Filmek> getAllFilmeks() {
		System.out.println("Osszes film.");

		List<Filmek> filmeks = new ArrayList<>();
		repository.findAll().forEach(filmeks::add);

		return filmeks;
	}

	@PostMapping(value = "/filmeks/create")
	public Filmek postFilmek(@RequestBody Filmek filmek) {

		Filmek _filmek = repository.save(new Filmek(filmek.getId(), filmek.getCim(), filmek.getMegjelenes(), filmek.getErtekeles()));
		return _filmek;
	}

	@DeleteMapping("/filmeks/{id}")
	public ResponseEntity<String> deleteFilmek(@PathVariable("id") long id) {
		System.out.println("Delete Filmek with ID = " + id + "...");

		repository.deleteById(id);

		return new ResponseEntity<>("Filmek has been deleted!", HttpStatus.OK);
	}

	@DeleteMapping("/filmeks/delete")
	public ResponseEntity<String> deleteAllFilmeks() {
		System.out.println("Delete All Filmeks...");

		repository.deleteAll();

		return new ResponseEntity<>("All filmeks have been deleted!", HttpStatus.OK);
	}

	@GetMapping(value = "filmeks/megjelenes/{megjelenes}")
	public List<Filmek> findByMegjelenes(@PathVariable int megjelenes) {

		List<Filmek> filmeks = repository.findByMegjelenes(megjelenes);
		return filmeks;
	}

	@PutMapping("/filmeks/{id}")
	public ResponseEntity<Filmek> updateFilmek(@PathVariable("id") long id, @RequestBody Filmek filmek) {
		System.out.println("Update Filmek with ID = " + id + "...");

		Optional<Filmek> filmekData = repository.findById(id);

		if (filmekData.isPresent()) {
			Filmek _filmek = filmekData.get();
			_filmek.setId(filmek.getId());
			_filmek.setCim(filmek.getCim());
			_filmek.setMegjelenes(filmek.getMegjelenes());
			_filmek.setErtekeles(filmek.getErtekeles());
			return new ResponseEntity<>(repository.save(_filmek), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
