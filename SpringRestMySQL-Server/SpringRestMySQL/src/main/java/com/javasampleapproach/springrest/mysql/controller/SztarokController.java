package com.javasampleapproach.springrest.mysql.controller;

import java.util.ArrayList;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.model.Sztarok;
import com.javasampleapproach.springrest.mysql.repo.SztarokRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class SztarokController {

	@Autowired
	SztarokRepository repository;

	@GetMapping("/sztaroks")
	public List<Sztarok> getAllSztaroks() {
		System.out.println("Osszes sztar.");

		List<Sztarok> sztaroks = new ArrayList<>();
		repository.findAll().forEach(sztaroks::add);

		return sztaroks;
	}

	@PostMapping(value = "/sztaroks/create")
	public Sztarok postSztarok(@RequestBody Sztarok sztarok) {

		Sztarok _sztarok = repository.save(new Sztarok(sztarok.getId(),sztarok.getNev(), sztarok.getSzulev(), sztarok.isOscardijas()));
		return _sztarok;
	}

	@DeleteMapping("/sztarok/{id}")
	public ResponseEntity<String> deleteSztarok(@PathVariable("id") long id) {
		System.out.println("Delete Sztarok with ID = " + id + "...");

		repository.deleteById(id);

		return new ResponseEntity<>("Sztarok has been deleted!", HttpStatus.OK);
	}

	@DeleteMapping("/sztarok/delete")
	public ResponseEntity<String> deleteAllSztaroks() {
		System.out.println("Delete All Sztaroks...");

		repository.deleteAll();

		return new ResponseEntity<>("All sztarok have been deleted!", HttpStatus.OK);
	}


	
}
