package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "cv")
public class Cv {
	
	@Id
	@Column (name="id")
	private long id;
	
	@Column (name="nev")
	private String nev;
	
	@Column (name="szul")
	private String szul;
	
	@Column (name="lakcim")
	private String lakcim;
	
	@Column (name="email")
	private String email;
	
	@Column (name="telefon")
	private String telefon;
	
	@Column (name="erdeklodes")
	private String erdeklodes;
	
	@Column (name="tanulmany")
	private String tanulmany;
	
	@Column (name="tanulmany2")
	private String tanulmany2;
	
	@Column (name="nyelv")
	private String nyelv;
	
	@Column (name="nyelv2")
	private String nyelv2;
	
	@Column (name="prognyelv")
	private String prognyelv;
	
	@Column (name="hobbi")
	private String hobbi;
	
	public Cv () {
		
	}
	
	public Cv (long id, String nev, String szul, String lakcim, String email, 
			String telefon, String erdeklodes, String tanulmany,  String tanulmany2, String nyelv, String nyelv2, 
			String prognyelv, String hobbi) {
		this.id = id;
		this.nev = nev;
		this.szul = szul;
		this.lakcim = lakcim;
		this.email = email;
		this.telefon = telefon;
		this.erdeklodes = erdeklodes;
		this.tanulmany = tanulmany;
		this.tanulmany2 = tanulmany2;
		this.nyelv = nyelv;
		this.nyelv2 = nyelv2;
		this.prognyelv = prognyelv;
		this.hobbi = hobbi;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getNev() {
		return nev;
	}
	
	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getSzul() {
		return szul;
	}

	public void setSzul(String szul) {
		this.szul = szul;
	}

	public String getLakcim() {
		return lakcim;
	}

	public void setLakcim(String lakcim) {
		this.lakcim = lakcim;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getErdeklodes() {
		return erdeklodes;
	}

	public void setErdeklodes(String erdeklodes) {
		this.erdeklodes = erdeklodes;
	}

	public String getTanulmany() {
		return tanulmany;
	}

	public void setTanulmany(String tanulmany) {
		this.tanulmany = tanulmany;
	}

	public String getNyelv() {
		return nyelv;
	}

	public void setNyelv(String nyelv) {
		this.nyelv = nyelv;
	}

	public String getPrognyelv() {
		return prognyelv;
	}

	public void setPrognyelv(String prognyelv) {
		this.prognyelv = prognyelv;
	}

	public String getHobbi() {
		return hobbi;
	}

	public void setHobbi(String hobbi) {
		this.hobbi = hobbi;
	}

	public String getTanulmany2() {
		return tanulmany2;
	}

	public void setTanulmany2(String tanulmany2) {
		this.tanulmany2 = tanulmany2;
	}

	public String getNyelv2() {
		return nyelv2;
	}

	public void setNyelv2(String nyelv2) {
		this.nyelv2 = nyelv2;
	}
	
	
	
	
	
	

}
