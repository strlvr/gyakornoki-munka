package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "filmek")
public class Filmek {

	@Id
	@Column(name = "id")
	private long id;

	@Column(name = "cim")
	private String cim;

	@Column(name = "megjelenes")
	private int megjelenes;

	@Column(name = "ertekeles")
	private double ertekeles;

	public Filmek() {
	}

	public Filmek(long id, String cim, int megjelenes, double ertekeles) {
		this.id = id;
		this.cim = cim;
		this.megjelenes = megjelenes;
		this.ertekeles = ertekeles;
	}
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCim() {
		return cim;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	public int getMegjelenes() {
		return megjelenes;
	}

	public void setMegjelenes(int megjelenes) {
		this.megjelenes = megjelenes;
	}

	public double getErtekeles() {
		return ertekeles;
	}

	public void setErtekeles(double ertekeles) {
		this.ertekeles = ertekeles;
	}



	@Override
	public String toString() {
		return "Filmek [id=" + id + ", cim=" + cim + ", megjelenes=" + megjelenes + ", ertekeles=" + ertekeles + "]";
	}
}
