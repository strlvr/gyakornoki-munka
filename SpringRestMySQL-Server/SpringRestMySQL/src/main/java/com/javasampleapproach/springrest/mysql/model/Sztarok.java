package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="sztarok")
public class Sztarok {
	

	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="nev")
	private String nev;

	@Column(name = "szulev")
	private int szulev;
	
	@Column(name = "Oscardijas")
	private boolean oscardijas;
	
	@Column(name = "fid")
	private int fid;
	
	public Sztarok() {
		
	}
	
	public Sztarok(long id, String nev, int szulev, boolean oscardijas){
		this.id = id;
		this.nev = nev;
		this.szulev = szulev;
		this.oscardijas = oscardijas;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public int getSzulev() {
		return szulev;
	}

	public void setSzulev(int szulev) {
		this.szulev = szulev;
	}

	public boolean isOscardijas() {
		return oscardijas;
	}

	public void setOscardijas(boolean oscardijas) {
		this.oscardijas = oscardijas;
	}
	
	@Override
	public String toString() {
		return "Sztarok [id=" + id + ", nev=" + nev + ", szulev=" + szulev + ", oscardijas=" + oscardijas + "]";
	}
	

}
