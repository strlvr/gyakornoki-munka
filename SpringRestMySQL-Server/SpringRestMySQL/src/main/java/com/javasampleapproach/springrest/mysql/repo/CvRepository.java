package com.javasampleapproach.springrest.mysql.repo;

import org.springframework.data.repository.CrudRepository;
import com.javasampleapproach.springrest.mysql.model.Cv;

public interface CvRepository extends CrudRepository <Cv, Long> {

}
