package com.javasampleapproach.springrest.mysql.repo;

import java.util.List;



import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.Filmek;


public interface FilmekRepository extends CrudRepository<Filmek, Long> {
	List<Filmek> findByMegjelenes(int megjelenes);
	
}

