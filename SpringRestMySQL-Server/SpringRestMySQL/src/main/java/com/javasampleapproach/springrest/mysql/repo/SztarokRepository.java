package com.javasampleapproach.springrest.mysql.repo;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.Sztarok;

public interface SztarokRepository extends CrudRepository<Sztarok, Long> {

	
}