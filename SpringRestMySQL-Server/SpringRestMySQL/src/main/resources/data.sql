LOCK TABLES `filmek` WRITE;
INSERT INTO `filmek` VALUES 
(4,'Vasember',2008,7.9),
(5,'Batman: Kezdődik',2005,8.3),
(10,'Forrest Gump',1994,8.8),
(20,'Egy új remény',1977,8.6),
(25,'A király visszatér',2003,8.9),
(30,'Ponyvregény',1994,8.9),(
35,'Tenacious D',2006,6.8);

LOCK TABLES `sztarok` WRITE;
INSERT INTO `sztarok` VALUES 
(100,'Tom Hanks','1956-07-09',1,10),
(104,'Harrison Ford','1942-07-13',0,20),
(106,'Mark Hamill','1951-09-25',0,20),
(108,'Carrie Fisher','1956-10-21',0,20),
(110,'Elijah Wood','1981-01-28',0,25),
(112,'Viggo Mortensen','1958-10-20',0,25),
(114,'Ian McKellen','1939-05-25',0,25),
(116,'John Travolta','1954-02-18',0,30),
(118,'Uma Thurman','1970-04-29',0,30),
(120,'Samuel L. Jackson','1948-12-21',0,30),
(122,'Jack Black','1969-08-28',0,35),
(124,'Kyle Gass','1960-07-14',0,35);
UNLOCK TABLES;

LOCK TABLES `cv` WRITE;
/*!40000 ALTER TABLE `cv` DISABLE KEYS */;
INSERT INTO `cv` VALUES (1,'Nagy Dávid','1995. október 7.','Békéscsaba, Viola utca 14.','p3rrypp@gmail.com','+30 425/2337','Informatika, azon belül weblapok létrehozása, fejlesztése.\nAngular, HTML, CSS3, Java.\nSzámítógépes hálózatok.','2007 - 2015  | Békéscsabai Evangélikus Gimnázium ','2016 -           | Szegedi Tudomány Egyetem, Gazdaságinformatikus','Német - B2 nyelv vizsga\n','Angol - B2 szinten ','C, C++ - alapok\nJava, PHP, HTML, CSS, Angular - medium\n ','Zenélés, mint pl. zongora vagy gitár.\nAsztali tenisz, görkorcsolya.');
/*!40000 ALTER TABLE `cv` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

