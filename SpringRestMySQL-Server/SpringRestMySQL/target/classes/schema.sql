CREATE DATABASE  IF NOT EXISTS `dtb5` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `dtb5`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: dtb5
-- ------------------------------------------------------
-- Server version	8.0.12


 SET NAMES utf8 ;


--
-- Table structure for table `filmek`
--
DROP TABLE IF EXISTS `cv`;
DROP TABLE IF EXISTS `sztarok`;
DROP TABLE IF EXISTS `filmek`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `filmek` (
  `id` int(11) NOT NULL,
  `cim` varchar(55) DEFAULT NULL,
  `megjelenes` int(11) NOT NULL,
  `ertekeles` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `sztarok`
--
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sztarok` (
  `id` int(11) NOT NULL,
  `nev` varchar(45) DEFAULT NULL,
  `szulev` date DEFAULT NULL,
  `Oscardijas` tinyint(4) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fid_idx` (`id`,`fid`),
  KEY `fid_idx1` (`id`),
  KEY `fid` (`fid`),
  CONSTRAINT `fid` FOREIGN KEY (`fid`) REFERENCES `filmek` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cv`
--
DROP TABLE IF EXISTS `cv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cv` (
  `id` int(11) NOT NULL,
  `nev` varchar(100) NOT NULL,
  `szul` varchar(100) DEFAULT NULL,
  `lakcim` varchar(300) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefon` varchar(100) DEFAULT NULL,
  `erdeklodes` varchar(300) DEFAULT NULL,
  `tanulmany` varchar(300) DEFAULT NULL,
  `tanulmany2` varchar(300) DEFAULT NULL,
  `nyelv` varchar(300) DEFAULT NULL,
  `nyelv2` varchar(300) DEFAULT NULL,
  `prognyelv` varchar(300) DEFAULT NULL,
  `hobbi` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
